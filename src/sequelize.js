const Sequelize = require("sequelize");
require("dotenv").config();
const { info } = require("./logger");

module.exports = function (app) {
  // const connectionString = app.get("mysql");
  const URI = `${process.env.DB_DIALECT}://${process.env.DB_USER}:${process.env.DB_PASSWORD}@${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.DB_NAME}`;
  console.info(URI);
  const sequelize = new Sequelize(URI, {
    dialect: "mysql",
    logging: false,
    define: {
      freezeTableName: true,
    },
  });

  sequelize
    .authenticate()
    .then(() => {
      console.log("DB connection established successfully");
    })
    .catch((err) => {
      console.log("Unable to connect to DB", err);
    });

  const oldSetup = app.setup;

  app.set("sequelizeClient", sequelize);

  app.setup = function (...args) {
    const result = oldSetup.apply(this, args);

    // Set up data relationships
    const models = sequelize.models;
    Object.keys(models).forEach((name) => {
      if ("associate" in models[name]) {
        models[name].associate(models);
      }
    });

    // Sync to the database
    app.set("sequelizeSync", sequelize.sync());

    return result;
  };
};
