# Docker Container Setup with Featherjs and MySQL

> 

## About

I created a basic feathers app with MySQL as the database and used it to create a docker container.

## Getting Started

### Clone Gitlab repository

```git clone git@gitlab.com:ArunMurali/docker-container-setup-for-feathers-and-mysql.git```

### Start containers

``` docker compose up -d ```

### Stop containers

```docker compose down ```


